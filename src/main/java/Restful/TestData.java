package Restful;

import java.util.HashMap;
import java.util.Map;

class TestData {
	
	  private long accountNumber;
	    private String firstName;
	    private String lastName;
	    private int noOfLines;
	    
	    public TestData()
	    {
	    	
	    }
	    
	    public TestData(long accountNumber,String firstName,String lastName,int noOfLines)
	    {
	    	this.accountNumber=accountNumber;
	    	this.firstName=firstName;
	    	this.lastName=lastName;
	    	this.noOfLines=noOfLines;
	    } 
	    
	    static TestData userAccounts(String accNo)
	    {
	    	HashMap<String,TestData> accounts=new HashMap<String,TestData>();
	    	accounts.put("1234567890", new TestData(1234567890,"Meredith","Grey",1));
	    	accounts.put("1234567891", new TestData(1234567891,"Mark","Sloan",2));
	    	accounts.put("1234567892", new TestData(1234567892,"Derek","Sheperd",3));
	    	return (TestData) accounts.get(accNo);
	    }
	    
		public long getAccountNumber() {
			return accountNumber;
		}
		public void setAccountNumber(long accountNumber) {
			this.accountNumber = accountNumber;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public int getNoOfLines() {
			return noOfLines;
		}
		public void setNoOfLines(int noOfLines) {
			this.noOfLines = noOfLines;
		}

		
 }

