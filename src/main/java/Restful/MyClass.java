package Restful;

import java.util.HashMap;

class MyClass {

	public static void main(String[] args) {
		HashMap<String,TestData> accounts=new HashMap<String,TestData>();
		TestData t1=new TestData(1234567890,"Meredith","Grey",1);
    	accounts.put("1234567890", t1);
    	accounts.put("1234567891", new TestData(1234567891,"Mark","Sloan",2));
    	accounts.put("1234567892", new TestData(1234567892,"Derek","Sheperd",3));
    	System.out.println(accounts.get("1234567891").getFirstName());

	}


}
