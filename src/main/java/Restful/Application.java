package Restful;

import org.apache.catalina.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
class Application {

	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		
	}
}
