package Restful;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/userData")
 class Controller {
	    @RequestMapping(method=RequestMethod.GET)
	    public TestData userData(@RequestParam(value="accNo")String accNo) {
	    return TestData.userAccounts(accNo);
	    }
}
